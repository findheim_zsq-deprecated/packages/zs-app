cfg = require '../config'
fetch = require '../util/fetch'
winston = cfg.winston()
_ = require 'lodash'
async = require 'async'

class ContentService
  @_cachedProps = {}
  @_cachedTypes = {}

  init: (callback, errcb) =>
    async.series [
      (cb) =>
        @reloadTypes(
          (types) =>
            cb()
          , (error) ->
            cb error
        )
      , (cb) =>
        @reloadProps(
          (props) =>
            cb()
          , (error) ->
            cb error
        )
      ], (error, results) =>
        if error?
          if errcb?
            errcb error
          else
            winston.error error
        else
          callback()

  reloadTypes: (cb, err) =>
    xtr = cfg.get 'endpoints.xtr'
    if xtr?
      fetch "#{xtr}metadata/getTypes", (res) =>
        @_cachedTypes = JSON.parse res
        winston.info "Reloaded #{_.size @_cachedTypes} types from #{xtr}metadata/getTypes"
        cb()
    else
      em = 'Extractor endpoint not set, content service cannot fetch type labels.'
      if err?
        err em
      else
        winston.error em

  reloadProps: (cb, err) =>
    xtr = cfg.get 'endpoints.xtr'
    if xtr
      fetch "#{xtr}metadata/getProperties", (res) =>
        @_cachedProps = JSON.parse res
        winston.info "Reloaded #{_.size @_cachedProps} properties from #{xtr}metadata/getProperties"
        cb()
    else
      em = 'Extractor endpoint not set, content service cannot fetch property labels.'
      if err?
        err em
      else
        winston.error em


  getTypes: () =>
    return @_cachedTypes

  getProps: () =>
    return @_cachedProps


  getTypeLabel: (key) =>
    return @_cachedTypes[key].label

  getPropLabel: (key) =>
    return @_cachedProps[key].label

module.exports = new ContentService()