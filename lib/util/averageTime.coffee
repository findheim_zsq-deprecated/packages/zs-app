class AverageTimer
  constructor: ->
    @totalTime = 0
    @count = 0
    @max = 0
    @min = Infinity

  startMeasuring: ->
    @_startTime = Date.now()

  stopMeasuring: (time) ->
    if time
      @_startTime = time
    if not @_startTime?
      throw new Error("Never started measuring!")
    took = Date.now() - @_startTime
    @totalTime += took
    @count++
    @max = took if took > @max
    @min = took if took < @min

  getAverage: ->
    @totalTime / @count

  getMax: ->
    @max

  getMin: ->
    @min

  getTotal: ->
    @totalTime

  toString: ->
    "avg: #{@getAverage()/1000.toPrecision(5)}s, total: #{@getTotal()/1000.toPrecision(10)}s, min: #{@getMin()/1000.toPrecision(5)}s, max: #{@getMax()/1000.toPrecision(5)}s"

  toJSON: ->
    JSON.stringify {count: @count, totalTime: @totalTime, avg: @getAverage(), max: @max, min: @min}

module.exports = AverageTimer