http = require 'http'
cfg = require '../config'
winston = cfg.winston()
url = require 'url'
_ = require 'lodash'

fetch = (theurl, success, errcb, data=null, method='GET', headers={}) ->
  # winston.debug "Fetching from #{theurl} with #{method} and data: #{data}, headers: #{JSON.stringify headers} ..."
  options = {}
  _.assign options, (url.parse theurl, '?' in theurl)
  options.headers = headers
  if data
    options.headers['Content-length'] = Buffer.byteLength data
  options.method = method.toUpperCase()

  doc = ''
  req = http.request options, (res) ->
    res.on "data", (chunk) ->
      doc += chunk
    res.on "end", () ->
      success doc, res

  if errcb?
    req.on 'error', errcb
  else
    req.on 'error', (err) ->
      winston.error "Failed to fetch #{url}: #{err}"

  if data
    req.write data
  req.end()

module.exports = fetch
