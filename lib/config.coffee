fs = require 'fs'

JSON.minify = require 'node-json-minify'
process = require 'process'
_ = require 'lodash'

winston = require "./logging"
env = require './env'

class Config
  constructor: () ->
    @_config = {}

  load: (overrides) =>
    # load config from base-config
    @loadConfig '../base-config/endpoints.coffee'

    # now load all .json files from the app's base config directory if exists/set
    cfgdir = env.get 'ZS_CONFIG_DIR', 'config'
    unless cfgdir.match /^\// then cfgdir = process.cwd() + '/' + cfgdir # if ZS_CONFIG_DIR not absolute
    if fs.existsSync cfgdir
      try
        for fn in fs.readdirSync cfgdir
          if fn.split('').reverse().join('').match /^(nosj|eeffoc)\.(?!elpmas)/ # reverse because neg look behind unsupported, nosj.elpmas.elif...
            @loadConfig cfgdir + '/' + fn
      catch error
        @_winston.error "Failed to load one or more config files from #{cfgdir}: #{error.message}"

    # apply overrides from ZS_CONFIG or arguments
    if process.env.ZS_CONFIG
      fn = process.env.ZS_CONFIG
      unless fn.match /^\// then fn = process.cwd() + '/' + fn
      @loadConfig fn
    if overrides?
      _.assign @_config, overrides
    return this

  winston: () ->
    winston

  loadConfig: (filename) =>
    try
      part = require filename
      _.assign @_config, part
      winston.info "Settings loaded from #{filename}: #{JSON.stringify(part)}"
    catch error
      winston.error "Loading config file '#{filename}' failed: #{error.message}."

  # Get config by key
  # @param [fqkey] fully qualified key (e.g. pg.servers.default)
  get: (fqkey, _default, silent=false) =>
    if not fqkey? or not fqkey then return
    cfg = @_config
    for k in fqkey.split '.'
      if k of cfg
        cfg = cfg[k]
      else
        if not silent and not _default?
          winston.error "Config for key #{fqkey} requested but no such config exists. Please set config in JSON config file."
        return _default || null
    return cfg

  pkgInfo: () =>
    unless @_pkginfo? # cache
      @_pkginfo = JSON.parse(fs.readFileSync process.cwd() + '/package.json', 'utf8')
    return @_pkginfo

unless global.zs_config?
  global.zs_config = new Config().load()
  winston.info "Environment: #{env.current.toUpperCase()}"
  winston.debug "using config from #{__dirname}"
  # init logging
  for entry in env.deferredLogs
    winston.log entry[0], entry[1]
else
  winston.debug "not using config from #{__dirname}, since config already is alive"

module.exports = global.zs_config