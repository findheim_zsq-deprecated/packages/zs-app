winston = require 'winston'
process = require 'process'

class Env

  constructor: () ->
    @DEVELOPMENT = 'development'
    @TESTING = 'testing'
    @PRODUCTION = 'production'
    @environments = [ @DEVELOPMENT, @TESTING, @PRODUCTION ]
    @current = null
    @_siletMode = false

    @deferredLogs = []
    if process.env.ZS_ENV
      if process.env.ZS_ENV in @environments
        @current = process.env.NODE_ENV = process.env.ZS_ENV
      else
        @deferredLogs.push [ "warn", "Invalid environment setting process.env.ZS_ENV: '#{process.env.ZS_ENV}', must be one of: #{@environments.join(', ')}." ]
    if not @current? and process.env.NODE_ENV
      if process.env.NODE_ENV in @environments
        @current = process.env.ZS_ENV = process.env.NODE_ENV
        @deferredLogs.push [ "info", "Using environment setting from process.env.NODE_ENV=#{process.env.NODE_ENV}." ]
      else
        @deferredLogs.push [ "warn", "Invalid environment setting process.env.NODE_ENV: '#{process.env.NODE_ENV}', must be one of: #{@environments.join(', ')}." ]
    if not @current?
      @current = @DEVELOPMENT
      @deferredLogs.push [ "warn", "Environment not set, using '#{@DEVELOPMENT}' as fallback. Please set ZS_ENV to one of #{@environments.join(', ')}." ]

  silentMode: (silent=undefined) =>
    if silent != undefined
      @_silentMode = silent
      if silent
        winston.warn "Silent mode enabled for zs-app/lib/env - errors on missing env values will be suppressed!"
    return @_silentMode

  get: (key, args...) => # an explicit _default null should be possible
    if key of process.env
      return process.env[key]
    else
      if args.length > 0
        return args[0]
      else
        unless @_silentMode
          e = new Error "Required environment variable not set: #{key}."
          winston.error e.message
          throw e
    return null

module.exports = new Env()