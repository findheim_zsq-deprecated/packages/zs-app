unless global.zs_winston?
  winston = global.zs_winston = require "./logging.coffee"
  winston.debug "using winston from #{__dirname}"
else
  winston = global.zs_winston
  winston.debug "not using winston from #{__dirname}, since winston already is alive"

module.exports = winston