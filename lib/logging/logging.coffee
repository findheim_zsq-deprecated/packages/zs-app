endpoints = require "../../base-config/endpoints"
winston = require('winston')
_ = require("lodash")
require('./winston-logstash-udp')
env = require '../env'
fs = require 'fs'

packageInfo = JSON.parse(fs.readFileSync(process.cwd() + '/package.json', 'utf8'))
transports =
  logstash: new (winston.transports.LogstashUDP)
    level: "notice"
    handleExceptions: true
    silent: env.current isnt env.PRODUCTION
    appName: if packageInfo?.name? then packageInfo.name else "missing package.json->name"
    appVersion: if packageInfo?.version? then packageInfo.version else "missing package.json->version"
    host: endpoints.endpoints.logging.host

  console: new (winston.transports.Console)
    level: if env.current is env.PRODUCTION then 'info' else 'trace'
    colorize: true
    prettyPrint: true
    silent: false
    timestamp: false
    handleExceptions: true

  file: new (winston.transports.File)
    level: if env.current is env.PRODUCTION then 'info' else 'debug'
    handleExceptions: true
    filename: env.get('ZS_LOGFILE', 'application.log')

levels=
  trace: 0
  debug: 1
  info: 2
  notice: 3
  warn: 4
  error: 5
  fatal: 6

winstonLogger = new (winston.Logger)
  levels: levels
  colors:
    trace: 'grey'
    debug: 'cyan'
    info: 'blue'
    notice: 'green'
    warn: 'yellow'
    error: 'red'
    fatal: 'red'
  transports: [
    transports.console
    transports.logstash
    transports.file
  ]

module.exports = winstonLogger