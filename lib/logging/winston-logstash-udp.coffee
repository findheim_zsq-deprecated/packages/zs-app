###
(C) 2013 Sazze, Inc.
MIT LICENCE

Based on a gist by mbrevoort.
Available at: https://gist.github.com/mbrevoort/5848179

Inspired by winston-logstash
Available at: https://github.com/jaakkos/winston-logstash

modified by SH, CS, zoomsquare
###

# Really simple Winston Logstash UDP Logger
dgram = require "dgram"
os = require "os"
winston = require "winston"
stackTrace = require 'stack-trace'

common = require "winston/lib/winston/common"

class LogstashUDP extends winston.Transport
  constructor: (options) ->
    super options
    options = options or {}

    @name = "logstashUdp"
    @level = options.level or "info"
    @host = options.host or "127.0.0.1"
    @source_host = options.source_host or os.hostname()
    @port = options.port or 9999
    @application = options.appName or process.title
    @appVersion = options.appVersion or 0
    @pid = options.pid or process.pid
    @client = null
    @connect()

  #
  # Define a getter so that `winston.transports.LogstashUDP`
  # is available and thus backwards compatible.
  #
  #
  connect : ->
    @client = dgram.createSocket("udp4")
    @client.unref()

  log: (level, msg, meta, callback) ->

    meta = winston.clone meta or {}

    if @silent
      callback(null, true)
    else
      if meta._origin?.source?
        sourceFile = meta._origin.source
      else
        sourceFile = ""
        # try and get source file
        trace = stackTrace.get()
        loggingpath = trace[0].getFileName().match /(.*)winston\-logstash\-udp\.coffee$/
        if loggingpath? and loggingpath.length >= 1
          serverpath = loggingpath[1].replace /logging.$/,''
          for item, index in trace
            if item.getFileName().match /node_modules.winston.lib.winston.common\.js/
              break

          if (index < trace.length + 1) and (trace[index+1]?)
            sourceFile = trace[index+1].getFileName().replace serverpath, ""
      if meta._module?
        module = meta._module
        delete meta._module
      else
        module = ""
      if meta._label?
        label = meta._label
        delete meta._label
      else
        label = ""

      fields =
        object: meta
        pid: @pid
        level: level
        application: @application
        app_version: @appVersion
        module: module
        source: sourceFile
        source_host: @source_host
        label: label

      logEntry =
        '@message': msg
        '@fields': fields
        '@timestamp': (new Date).toISOString()

      @sendLog logEntry, (err,res) =>
        unless err?
          @emit "logged", true
          callback(null, true)
        else
          callback(err, false)

  sendLog: (message, callback) ->
    #console.log message
    buf = new Buffer(JSON.stringify(message))
    callback = (callback or ->
    )
    @client.send buf, 0, buf.length, @port, @host, callback

winston.transports.LogstashUDP = LogstashUDP
module.exports.LogstashUDP = LogstashUDP