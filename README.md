# zs-app base lib

** The base lib for zoomsquare apps written in node. **

The zs-app module is a collection of leightweight node libs for general tasks such as environments, config management, logstash logging (winston config), utils and general services).

## Why?

- Ultra-fast starting point for new apps
- Common config, logging, testing, docu, ... patterns
- Environment management (production/testing/development)
- Shared base-config with zoomsquare settings
- No passwords in git
- All dependent apps benefit from general (tested) improvements of zs-app

## Setup and usage

1. `npm install zs-app` or create a new module and add `zs-app` to `package.json`
   
    * add the following line to your dependencies (master is always stable):
    
    		"zs-app": "git+ssh://git@gitlab.zoomsquare.com:packages/zs-app.git"
    	
    * Note that on Unix/Mac you can also `git clone git@gitlab.zoomsquare.com:packages/zs-app.git` and use [npm-link](https://www.npmjs.org/doc/cli/npm-link.html). This is useful when extending the base modules because by symlinking the files immediately change within `node_modules`.

2. Use:

		cfg = require 'zs-app/lib/config'


## Basics

### Environments

Each zoomsquare app may run locally (while _developing_), on a _test_ server, or in _production_ on a live server. It is **very important** that each instance is started with the correct environment setting.

We use three environments:

* `development`:    local development
* `testing`:     	running on a test system (e.g. test pipeline for backend services)
* `production`:  	production environment on a live server

The environment is set by the environment variable `ZS_ENV` (or `NODE_ENV`) and is managed by `zs-app/lib/env`: Use it to query the `current` environment and `get` environment variables:

		env = require 'zs-app/lib/env'
		if env.current is env.PRODUCTION
			neverTellOur(env.get 'ZS_SECRET_ENV')

#### Adapt app to environments

Apps should be written in a way that critical settings adapt to the environment. In a production environment the configuration is usually different than when developing locally or testing the app.

Test cases should succeessfully run in development and/or testing environment, and _should fail_ in production to prevent destruction of any databases (add a check on top of criticial test suites).


### Configurations

Configuration is done using config files and environment variables. Basically, there are three kinds of settings:

* global zoomsquare settings
	* everything that should be consistent across all servers
	* e.g. service endpoints
	* they are part of zs-app and reside in `base-config/*`
	* added to git
* default settings bundled with your app
	* all parameters that should be configurable
	* just place files into `<your-app>/config/` and they will be loaded automatically
	* the directory can be changed with the environment variable `ZS_CONFIG_DIR`
	* added to git
* local settings for an app instance
	* not part of git and not bundled with the app
	* path to filename is set with the environment variable `ZS_CONFIG`
	* these settings may override the defaults

You can use `.json`, `.coffee`, or `.js` for all config files (use `module.exports` when using dynamic `.js` or `.coffee` scripts to return a JSON object). Files within `<your-app>/config/*` are automatically loaded.

#### Loading order

Configuration is managed by `zs-app/lib/cfg`. Once it is loaded with require, the zoomsquare global configs will be loaded first, then the default settings of your app residing in the `config/` directory of the app root are loaded, and if `ZS_CONFIG` is set, this file is loaded as well. Any configs from files loaded later will override existing configs with equal keys.

#### Passwords and sensitive information

** Important: **  
Any sensitive setting MUST NOT be added to your config files directly and added to the git repo. Use environment variables and the `env` lib instead as shown below. Not only passwords, but also secret keys or third party API keys are sensitive information.

#### Example config file

E.g. a `config/pg.coffee` might look like this:

    env = require 'zs-app/lib/env'
    
    module.exports = pg:
      servers:
        default:
          database: "bigdatamaps_server"
          host: switch env.current
            when env.PRODUCTION then "postgres.zoomsquare.com"
            when env.TESTING then "postgres.tp.zoomsquare.com"
            when env.DEVELOPMENT then "localhost"
          user: "zoomsquare"
          password: env.get("ZS_PG_PASSWORD")
          poolSize: 10

The reason why we don't use `process.env` is, that `zs-app/lib/env` will do some extra-work and throw exceptions on missing variables. You can also specify a default value as second argument and `null` to suppress the error.

#### Access config values

The final app config is a big JSON object with cascaded key, value pairs. Each config file will expand that JSON object. Config files loaded later will overide keys from previously loaded config files. You can use values but also JSON objects as values for your configs.

In order to access configs use:

	cfg = require 'zs-app/lib/config'
	
	cfg.get 'pg.serveres.default.database'
	cfg.get 'unknown.key'	# throws an error
	cfg.get 'unknown.key', 'but with a default value'
	cfg.get 'unknown.key', null, true	# be silent and don't throw error

Note the quotes: you don't specify the direct object identifier, but a fully qualified string to the sub-object.

#### Access package info

You can access the values from your package.json with `cfg.pkgInfo()`. This is useful to print version IDs or the name of the running app somewhere into logs or on a website.

Example:

	cfg = require 'zs-app/lib/config'
	v = cfg.pkgInfo().version


## Logging

We use winston for logging. Please NEVER use console.log, since logs cannot be directed to other targets this way. Winston is automatically configured with zoomsquare settings once you load `zs-app/lib/config`. Use the reference from cfg.winston() to ensure the global winston object is used:

    cfg = require 'zs-app/lib/config'
    winston = cfg.winston()
    winston.info 'A log message'

Logging levels: 

* trace
* debug
* info
* **notice**
* **warn**
* **error**
* **fatal**

**bold** levels are logged to logStash

Transports:

* **Console** logs all levels (in production: `info` and higher)
* **File** logs `debug` and higher (in production: `info` and higher); `fileName` can be set with env `ZS_LOGFILE` or defaults to `application.log`
* **LogStash** logs only in production `notice` and higher

### Meta data

Winston supports meta data, which is useful for logs in logStash to contain more info. Everything in the meta data will be found at `@fields.object`

Usage:

		winston.notice 'a log message', { moreInfo: 'a lot more Info' }
    
There are some reserved keywords in the meta data, which you can use for easy filtering in logStash:

* `_module`: the name of the module, like "server" or "client" (found at `@fields.module`)
* `_origin.source`: you can specifiy the source of the error, like filename or path (found at `@fields.source`)
* `_label`: a label to identify the log message, useful for messages that can be logged by more than one application (found at `@fields.label`)

The application name and version are taken from package.json, please make sure you have at least these two entries: 

		{ "name": "application name", "version": "0.0.4" }

found at `@fields.application` and `@fields.app_version`

If you want to override the logging configuration, just override the winston configuration at the start of your program.


## Test framework / best practices

We use `mocha` for testing apps: have a look into the test directory to get the idea behind mocha. Tests are run with npm which runs the mocha (see scripts in package.json):

		npm test

## Code documentation

We use [Codo](https://github.com/coffeedoc/codo) for docs generation.

Each app must contain a `readme.md` within the root folder with a quick How-to and a basic deployment guide.

## Dependencies

In general, try to keep the number of dependencies as low as possible! All dependencies should be up-to-date within package.json and added to the git repo. Any further dependencies including build guies must be mentioned in the `readme.md`.


## Utils bundled with zs-app

There are some general utils used by many zoomsquare apps in `zs-app/lib/util`. Don't add utils that are hardly used or can be added as npm package.

## Services bundled with zs-app

There are also several HTTP client services in `zs-app/lib/service`.

### Content service (types, properties)

Loaded type and property labels from the XTR ontology. Requires the config entry `endpoints.extractor` (e.g. `http://xtr.zoomsquare.com:8080/`):

    content = require 'zs-app/lib/service/content'
    content.init () ->
		apt = content.getTypeLabel 'Apartment'
		ren = content.getPropLabel 'renovated'
