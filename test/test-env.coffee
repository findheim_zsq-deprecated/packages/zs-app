assert = require 'assert'
process = require 'process'
env = require '../lib/env'
cfg = require '../lib/config'
winston = cfg.winston()

describe 'env', () ->
  it 'should return the current environment set with ZS_ENV', () ->
    if process.env.ZS_ENV or process.env.NODE_ENV
      assert env.current in [ process.env.ZS_ENV, process.env.NODE_ENV ], "Environment was not equal to process.env.ZS_ENV or NODE_ENV, it was: #{env.current}"
    else
      assert env.current is 'development', "Environment didn't fallback to default 'development'"

  it 'should return environment variable TEST_VARIABLE', (done) ->
    process.env.TEST_VARIABLE = 'test'
    assert.equal env.get('TEST_VARIABLE'), 'test'
    delete process.env.TEST_VARIABLE
    done()

  it 'should return the specified default value for not existing environment variable', () ->
    assert.equal env.get('__VARIABLE_DOES_NOT_EXIST_FOR_SURE', 'default value'), 'default value'

  it 'should throw an error if no environment variable and no default value exist', () ->
    try
      env.get '__VARIABLE_DOES_NOT_EXIST_FOR_SURE'
      assert false, 'Should have thrown an error'
    catch err
      assert.equal err.message, "Required environment variable not set: __VARIABLE_DOES_NOT_EXIST_FOR_SURE."

  it 'should return null if default value is null (silent mode)', () ->
    try
      assert.equal env.get('__VARIABLE_DOES_NOT_EXIST_FOR_SURE', null), null
    catch err
      assert false, 'Should not have thrown an error because default value was specified (null).'
