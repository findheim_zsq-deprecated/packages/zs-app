t = require "../lib/util/exectimers2.coffee"

tick = new t.Tick "name"

tick.start()

setTimeout ->
  tick.stop()
  console.log t.timers["name"]
  console.log "diff", tick.getDiff()
  console.log "duration", t.timers["name"].duration()
  console.log "mean", t.timers["name"].mean()
  console.log "min", t.timers["name"].min()
  console.log "max", t.timers["name"].max()
  t.statsRoute()(
    null,
      json: (arg) ->
        console.log "response:", arg
  )
, 1000