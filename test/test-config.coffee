assert = require 'assert'
cfg = require '../lib/config'

describe 'config', () ->
  it "should return app-level configs from config/ or ZS_CONFIG_DIR", (done) ->
    # !!CAUTION!! runtime modification of env and cfg - might have side effects
    #process.env.ZS_CONFIG_DIR = 'test/config'
    #cfg.load()
    # now loading with ZS_CONFIG_DIR set in package.json test script
    assert.equal (cfg.get '__some.__other'), 'settings'
    assert.equal (cfg.get '__and.__further'), 'infos'
    done()

  it "should return a default config if not found", () ->
    assert.equal (cfg.get 'asvawefwf', 'DEFAULT', true), 'DEFAULT'