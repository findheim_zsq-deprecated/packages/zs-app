env = require '../lib/env'
process = require 'process'
cfg = require '../lib/config'
winston = cfg.winston()

if env.current? and env.current is env.PRODUCTION
  winston.error 'Not allowed to execute tests in production environment.'
  process.exit()
