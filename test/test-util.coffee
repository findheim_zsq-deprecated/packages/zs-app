assert = require 'assert'
process = require 'process'
http = require 'http'
fetch = require '../lib/util/fetch'
cfg = require '../lib/config'
winston = cfg.winston()

describe 'util', () ->

  describe 'fetch', () ->
    server = null
    before () ->
      server = http.createServer (req, res) ->
        data = ''
        req.on 'data', (chunk) ->
          data += chunk
        req.on 'end', () ->
          res.writeHead 200, { 'Content-Type': 'text/plain' }
          res.end "TEST CONTENT #{data}"
      server.listen 9615

    after () ->
      if server
        server.close

    it 'should fetch document via HTTP from mock server', (done) ->
      fetch 'http://localhost:9615/', (doc, res) ->
        assert.equal res.statusCode, 200
        assert.equal doc.slice(0, 12), 'TEST CONTENT'
        done()
      , (err) ->
        assert false, err
        done()

    it 'should call error cb on error', (done) ->
      fetch 'http://unknownhostorso/', (doc, res) ->
        assert false, 'Should return an error.'
        done()
      , (err) ->
        assert.equal err.code, 'ENOTFOUND'
        done()

    it 'should echo GET data sent along with the request', (done) ->
      fetch 'http://localhost:9615/', (doc, res) ->
        assert.equal res.statusCode, 200
        assert.equal doc, 'TEST CONTENT WITH SOME DATA'
        done()
      , (err) ->
        assert false, err
        done()
      , 'WITH SOME DATA', 'get'

    it 'should echo POST data sent along with the request', (done) ->
      fetch 'http://localhost:9615/', (doc, res) ->
        assert.equal res.statusCode, 200
        assert.equal doc, 'TEST CONTENT WITH SOME DATA'
        done()
      , (err) ->
        assert false, err
        done()
      , 'WITH SOME DATA', 'post'

  describe 'set', () ->
    # TODO
