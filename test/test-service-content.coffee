assert = require 'assert'
process = require 'process'
content = require '../lib/service/content'
cfg = require '../lib/config'
winston = cfg.winston()

describe 'service', () ->

  describe 'content', () ->

    before (done) ->
      content.init () ->
        done()

    describe 'getTypeLabel', () ->
      it 'should return type label for Apartment', () ->
        assert.equal (content.getTypeLabel 'Apartment'), 'Wohnung'
        assert.equal (content.getTypeLabel 'House'), 'Haus'

    describe 'getPropLabel', () ->
      it 'should return property label for renoviert', () ->
        assert.equal (content.getPropLabel 'renovated'), 'renoviert'
