env = require '../lib/env'

module.exports =
endpoints:
  xtr: switch env.current
    when env.PRODUCTION then "http://xtr.zoomsquare.com:8080/"
    when env.TESTING then "http://xtr.tp.zoomsquare.com:8080/"
    else "http://localhost:8080/"
  bdm: switch env.current
    when env.PRODUCTION then "http://bdm1.zoomsquare.com/"
    when env.TESTING then "http://bdm.tp.zoomsquare.com/"
    else "http://localhost:8000/"
  logging:
    host: "logging.zoomsquare.com"