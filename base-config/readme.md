### base-config files

These files are bundled with zs-app and loaded using require, i.e. use `module.exports = ...`

**Caution**: zs-app might be included in frontend code. Therefore, please do not add sensitive config values here and only add config values that might otherwise reside in client side code as well.
